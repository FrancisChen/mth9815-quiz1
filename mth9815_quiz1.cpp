//
//  mth9815_quiz1.cpp
//
//  Created by Huiyou Chen on 11/07/16.
//  Copyright � 2016 Francis Chen. All rights reserved.
//
#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>

void quickSort(std::vector<int>& arr, int left, int right){
    if(arr.size() > 1){
        int i = left; int j = right;
        int pivot = arr[(i+j)/2];
        
        while(i<=j){
            while(arr[i] < pivot) ++i;
            while(arr[j] > pivot) --j;
            if(i<=j){
                std::swap(arr[i], arr[j]);
                ++i;
                --j;
            }
        }
        if(left<j) quickSort(arr,left,j);
        if(i<right) quickSort(arr,i,right);
    }
}
void Test_quickSort(){
    std::vector<int> v{9, 8, 7, 6, 5, 4, 3, 2, 1};
    quickSort(v,0,8);
    for(auto x : v) {std::cout << x << ", ";}
    std::cout<< "\n";
}

template<typename T>
class MaxHeap {
private:
    std::vector<T> heap;
    int heap_size;
    int parent(int i) {return (i - 1) / 2;} // returns index of parent (no bounds checking)
    int left(int i) {return 2 * i + 1;} // index of left child (no bounds checking)
    int right(int i) {return 2 * i + 2;}    // index of right child (no bounds checking)
public:
    // default constructor just uses vector<T> default constructor (heap is empty)
    MaxHeap();
    ~MaxHeap(){}
    // constructor from vector
    MaxHeap(std::vector<T> v);
    // constructor from array
    MaxHeap(T arr[], size_t size);
    void Print();
    void Up();
    void Add(const T key);
    void Remove() throw(std::underflow_error);
    MaxHeap<T>& operator=(const MaxHeap<T>&);
};

template<typename T>
MaxHeap<T>::MaxHeap(){
    std::vector<T> heap;
    heap_size = 0;
}
// constructor from vector
template<typename T>
MaxHeap<T>::MaxHeap(std::vector<T> v) {
    heap = v;
    heap_size = v.size();
    Up();
}
// constructor from array
template<typename T>
MaxHeap<T>::MaxHeap(T arr[], size_t size) {
    std::vector<T> v(arr, arr + size);
    heap = v;
    heap_size = v.size();
    Up();
}

template<typename T>
MaxHeap<T>& MaxHeap<T>::operator=(const MaxHeap<T>& h) {
    heap = h.heap;
    return h;
}

template<typename T>
void MaxHeap<T>::Print() {
    size_t size = heap.size();
    int i;
    std::cout << "<";
    if (size > 0) {
        for (i = 0; i < size - 1; ++i) {
            std::cout << heap[i] << ", ";
        }
        std::cout << heap[i];
    }
    std::cout << ">" << "\n";
    
}

template<typename T>
void MaxHeap<T>::Up() {
    if(heap_size>0){
        int start_index = heap_size - 1;
        while (start_index > 0 && (heap[start_index] > heap[parent(start_index)])) {
            std::swap(heap[start_index] , heap[parent(start_index)]);
            start_index = parent(start_index);
        }
    }
}

template<typename T>
void MaxHeap<T>::Add(T key) {
    heap.push_back(key);
    ++heap_size;
    Up();
}

template<typename T>
void MaxHeap<T>::Remove() throw(std::underflow_error) {
    if (heap_size==0) throw std::underflow_error("Heap is empty!");
    std::swap(heap[0], heap[heap_size-1]);
    heap.pop_back();
    Up();
}

void Test_MaxHeap(){
    MaxHeap<int> myHeap;
    for(int i=1; i<=9; ++i){
        myHeap.Add(i);
    }
    myHeap.Print();
    myHeap.Remove();
    myHeap.Print();
}

int main(int argc, const char * argv[]) {
    
    Test_quickSort();
    Test_MaxHeap();
    return 0;
}
